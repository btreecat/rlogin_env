Tools script for rlogin
=======================

Thank you for your interest in my script!

This script is meant to set up a few tools I feel are missing from the rlogin 
environment. Namely, tmux, mercurial (hg), and a decent default vim 
configuration. 

Installation
------------

Before running this script there are a few things you should check.

1. Do you have any of these files in your home directory? (use 'ls -la')
    * .tmux.conf
    * .hgrc
    * .vimrc
    * .bash_profile
    * .bashrc

2. Are the contents of those files important?
    * YES, move, make a backup, or delete them. This script will OVERWRITE to these files. We want to avoid duplicate or conflicting settings in our config files. You can merge them later if needed.
    * NO, or if you DELETED them because they were mostly empty you may proceed.

3. Download the tools.sh script. :: 

    $ wget https://bitbucket.org/btreecat/rlogin_env/raw/tip/tools.sh

4. Make the tools.sh script executable. ::

    $ chmod +x tools.sh

5. Run the script. ::

    $ ./tools.sh

6. After everything completes, you will need to log out, then back in again. This is because we created/updated a .bash_profile config file. This is read by bash when you ssh into rlogin (or any \*nix box really). 

7. After logging back in you should test everything.
    * Does tab completion work for hg and tmux? 
        * Try typing 'h' or 'tm' and hitting the Tab key twice
    * Can you clone with out issue?
        * hg clone https://bitbucket.org/btreecat/rlogin_env
    
Additional Info
----------------

If you have never used a DVCS like mercurial or git before, check out http://hginit.com for a tutorial and some basic concepts. 

Some basic tmux commands(while in tmux)

=================== =========================================
Command             Action
=================== =========================================   
Ctrl-B D            Detaches from session
Ctrl-B C            Creates a new tab in current session
Ctrl-B N            Switches to next tab in current session
Ctrl-B -            Splits the current tab horizontally
Ctrl-B |            Splits the current tab vertically 
Ctrl-B <arrow key>  Moves focus between splits
=================== =========================================
    
Reattach tmux to a disconnected session ('a' can be used as short hand) ::

    tmux a[ttach]              
    
Why is this useful? Say you are in a gdb session, and you lose wifi.
rlogin may then disconnect you. What happened to all your progress in gdb?
It's gone. Your terminal session was killed off! What cant you do about it?
Use tmux! First launch tmux to get a fresh session. Here you can then do
your editing in vim or work in gdb. If you need to stop and come back later
or if you lose Internet connection, you can attach to your previous session
after you ssh back to rlogin. This then lets you pick right up where you
left off!

One thing to note with the rlogin enviornment. When you ssh back in after disconnecting (intentional or not)
you will likely end up on a different host that your previous session. To remedy this, pay attention to the 
host name on the machine you access next time. Once you know what it is, you can ssh to it from any of the 
other rlogin hosts. This way you will always land back on the machine running your tmux session.

tmux FTW!

For everything you could possibly want to know about tmux and links to
even more, check out:
https://wiki.archlinux.org/index.php/Tmux

To bring up the list of sugguested words in vim, highlight the word and
use the command z= then just follow the directions on screen. 

Share and enjoy!