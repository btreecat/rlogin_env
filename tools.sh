#!/bin/bash

# author : Stephen Tanner <btreecat@stephentanner.com>

# Copyright Notice
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General Public 
# License.
# Share and enjoy!
#==============================================================================

#============================ Setup ===========================================

# Create directories
mkdir -p $HOME/local $HOME/tools_tmp

# Make sure there is a .bash_profile and a .vimrc 
touch $HOME/.bash_profile
touch $HOME/.bashrc
touch $HOME/.vimrc

# Get into position
TOOLS=$HOME/tools_tmp
cd $TOOLS

#============================ Mercurial =======================================

# First download mercurial (hg)
wget http://mercurial.selenic.com/release/mercurial-3.1.1.tar.gz
tar xvzf mercurial-3.1.1.tar.gz
cd mercurial-3.1.1

# Install hg w/o documentation 
# This is because it would require building/installing more dependencies.
# Google, elinks and tmux are your friends =D
make install-home-bin 
echo "Mercurial (hg) now installed."
cd $TOOLS


# Add certs to ~/.hgrc
# This is specific to REHL and its derivatives. 
touch $HOME/.hgrc
cat > $HOME/.hgrc << "EOF"
[web]
cacerts = /etc/pki/tls/certs/ca-bundle.crt

[ui]
username = user <user@domain.tld>

[extensions]
color = 
progress = 
EOF
echo ".hgrc has been updated."

#============================ tmux ============================================

# Next install libevent
# This is a dependency for tmux
wget https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz
tar xzvf libevent-2.0.21-stable.tar.gz
cd libevent-2.0.21-stable
./configure --prefix=$HOME/local --disable-shared
make
make install
echo "libevent now installed."
cd $TOOLS

# Finally install tmux
wget http://downloads.sourceforge.net/tmux/tmux-1.9a.tar.gz
tar zxfv tmux-1.9a.tar.gz
cd tmux-1.9a
./configure CFLAGS="-I$HOME/local/include" LDFLAGS="-L$HOME/local/lib -L$HOME/local/include"
CPPFLAGS="-I$HOME/local/include" LDFLAGS="-static -L$HOME/local/include -L$HOME/local/lib" make
cp tmux $HOME/bin
echo "tmux now installed."
cd $TOOLS

# Add | and - key bindings to tmux for easy tiled terms
touch $HOME/.tmux.conf
cat > $HOME/.tmux.conf << "EOF"
unbind %
bind | split-window -h
bind - split-window -v 
EOF
echo ".tmux.conf has been updated."

#============================ vim =============================================

# Make vim suck less
cat > $HOME/.vimrc << "EOF"
set autoindent smartindent      " auto/smart indent
set autoread                    " watch for file changes
set cursorline                  " show the cursor line
set number                      " line numbers
set shiftwidth=4                " Use 4 spaces not 8
set tabstop=4                   " ^^^^^^^^^^^^^^^^^^
set softtabstop=4               " ^^^^^^^^^^^^^^^^^^
set smarttab                    " tab and backspace are smart
set expandtab                   " convert tab chars to space chars
set showmatch                   " show matching bracket
set background=dark             " use colors better for a terminal with a dark bg color
filetype indent plugin on       " detect proper indent scheme
syntax on                       " enable syntax highlighting
set spell spelllang=en_us       " enable spell check
set textwidth=80
set wrapmargin=2
EOF
echo ".vimrc has been updated."

#============================ Finish ==========================================

# Add programs to path
cat > $HOME/.bash_profile << "EOF"
if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi
EOF
echo ".bash_profile has been updated."

#Need to also add to bash rc so you can clone from your remote repos.
cat > $HOME/.bashrc << "EOF"
# Add local python to pythonpath
export PYTHONPATH=${HOME}/lib64/python
# Add hg and tmux commands to path
export PATH=${HOME}/bin:$PATH
# Add alias for tmux to support 256color
alias tmux='tmux -2'
PS1='[\u@\h \W]\$ ' 
EOF
echo ".bashrc has been updated."

#remove temp files
cd $HOME
echo "cleaning up..."
rm -rf $TOOLS

echo "All done!"

